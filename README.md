# 高速交易总线（ATH）[申请试用](http://developer.archforce.cn/activity/apply/?productType=ATH)

## 介绍
高速交易总线，基于低时延分布式消息通信基础，面向开放协议（Protocol Buffers），提供交易网关增强，实时数据服务，友商适配服务等交易核心系统走向敏捷开放的关键组件。ATH性能卓越，百万级吞吐，百微秒级时延。

## 聚焦领域

**交易系统多样化，如何更便捷接入** 	
- 现货、两融、期权、快速版、极速版、超极速、股转、理财 .... 
- 协议相对统一，平衡性能与易用性
- 如何动态路由，支持7x24

**交易通道与数据服务分离，如何提供全资产实时数据服务** 
- 客户视野面向全资产（总市值，总持仓，所有交易记录）
- 客户服务面向公司级（开户，CRM，服务通知... )

**如何兼容厂商升级** 
- 北向接入服务，TdxApi，T2,  KCXP ....
- 南向服务对接，T2，KCBP ...



## 软件架构

![ATH蓝图](https://images.gitee.com/uploads/images/2020/1211/170337_1a5e38b0_7458.png "ATH蓝图.png")


### 交易网关增强（XGW）
![XGW](https://images.gitee.com/uploads/images/2020/1210/172550_5ccfa950_7458.png "交易网关增强（XGW）【窄】.png")

### 实时数据服务（RDS）
![RDS](https://images.gitee.com/uploads/images/2020/1210/172659_e8fa9a10_7458.png "交易数据服务（RDS）【窄】.png")

### 友商接入适配（CAS）
![CAS](https://images.gitee.com/uploads/images/2020/1210/172817_a9b2f087_7458.png "友商接入适配（CAS）【窄】.png")


## 关于高可用
![两地三中心](https://images.gitee.com/uploads/images/2020/1210/172948_2b69eb96_7458.png "两地三中心.png")